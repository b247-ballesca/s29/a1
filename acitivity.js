// 1


	db.users.find({$or: 
		[
			{firstName: {$regex: 's', $options: '$i'} },
			{lastName: {$regex: 'd', $options: '$i'} }
			]},
		{firstName: 1, lastName: 1}
	)

//2

db.users.find({  $and: [
	{ department: {$regex: 'HR', $options: '$i'} } , 
	{ age: {$gt: 70} }
	] 
});

//3
 db.users.find({  $and: [
	{ firstName: {$regex: 'e', $options: '$i'} } , 
	{ age: {$lt: 30} }
	] 
});